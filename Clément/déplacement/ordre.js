const mineflayer = require('mineflayer')
const { pathfinder, Movements, goals } = require('mineflayer-pathfinder')
const vec3 = require('vec3')
const GoalFollow = goals.GoalFollow
const GoalBlock = goals.GoalBlock
const GoalHome = goals.GoalBlock
var state = 1
const bot = mineflayer.createBot({
  host: '86.221.243.209', // minecraft server ip
  username: 'Bot-Clemonstre', // minecraft username
  auth: 'offline' // for offline mode servers, you can set this to 'offline'
})

bot.loadPlugin(pathfinder)

function followPlayer(){
    const playerCI = bot.players['XcoMaNdSs']
    if (!playerCI){
        bot.chat("wsh tu veut que je follow qui la")
        return
    }
    const mcData = require('minecraft-data')(bot.version)
    const movements = new Movements(bot, mcData)
    bot.pathfinder.setMovements(movements)
    movements.scafoldingBlocks =[]
    movements.scafoldingBlocks.push(mcData.blocksByName.dirt.id)

    const goal = new GoalFollow(playerCI.entity , 1)
    bot.pathfinder.setGoal(goal,true)
}

function locateIron(){
    state=1
    const mcData = require('minecraft-data')(bot.version)
    const movements = new Movements(bot, mcData)
    bot.pathfinder.setMovements(movements)
    movements.scafoldingBlocks =[]
    movements.scafoldingBlocks.push(mcData.blocksByName.dirt.id)

    const IronBlock = bot.findBlock({
        matching : mcData.blocksByName.iron_ore.id,
        maxDistancce : 32
    })
    if (!IronBlock){
        bot.chat("no iron ore in 32 block")
        return
    }
    const x =IronBlock.position.x
    const y =IronBlock.position.y + 1
    const z =IronBlock.position.z
    const goalIron = new GoalBlock(x ,y ,z)
    bot.pathfinder.setGoal(goalIron)
}

function returnHome(){
    const mcData = require('minecraft-data')(bot.version)
    const movements = new Movements(bot, mcData)
    bot.pathfinder.setMovements(movements)
    movements.scafoldingBlocks =[]
    movements.scafoldingBlocks.push(mcData.blocksByName.dirt.id)

    const home = bot.findBlock({
        matching : mcData.blocksByName.coal_block.id,
        maxDistancce : 256
    })
    if (!home){
        bot.chat("no home in 256 block")
        return
    }
    const x =home.position.x
    const y =home.position.y + 1
    const z =home.position.z
    const goalHome = new GoalHome(x ,y ,z)
    bot.pathfinder.setGoal(goalHome)
}


bot.on('chat', (username, message) => {
    if (username=="XcoMaNdSs"){
        if (message=="fer"){
            locateIron()
            bot.chat("je cherche du fer")
        }else if(message=="stop"){
            bot.pathfinder.stop()
            bot.chat("ok")
        }else if(message=="moi"){
            followPlayer()
            bot.chat("je te suis")
        }else if(message=="home"){
            returnHome()
            bot.chat("je rentre")
        }else if(message=="mine"){
                mineSTP()
                bot.chat("je mine")
        }else{
            bot.chat("pas compris")
        }
    }
  })


  async function mineSTP(){
    target = bot.blockAt(bot.entity.position.offset(0, -1, 0))
    if (target && bot.canDigBlock(target)) {
        bot.chat(`starting to dig ${target.name}`)
        try {
          await bot.dig(target)
          bot.chat(`finished digging ${target.name}`)
        } catch (err) {
          console.log(err.stack)
        }
      }
}