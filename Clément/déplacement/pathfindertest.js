const mineflayer = require('mineflayer')
const { pathfinder, Movements, goals } = require('mineflayer-pathfinder')
const GoalFollow = goals.GoalFollow
const bot = mineflayer.createBot({
  host: '86.221.243.209', // minecraft server ip
  username: 'Bot-Clémonstre', // minecraft username
  auth: 'offline' // for offline mode servers, you can set this to 'offline'
})

bot.loadPlugin(pathfinder)

function followPlayer(){
    const playerCI = bot.players['XcoMaNdSs']
    if (!playerCI){
        bot.chat("wsh tu veut que je follow qui la")
        return
    }
    const mcData = require('minecraft-data')(bot.version)
    const movements = new Movements(bot, mcData)
    bot.pathfinder.setMovements(movements)
    movements.scafoldingBlocks =[]
    movements.scafoldingBlocks.push(mcData.blocksByName.dirt.id)

    const goal = new GoalFollow(playerCI.entity , 1)
    bot.pathfinder.setGoal(goal,true)
}

bot.once('spawn',followPlayer)