const mineflayer = require('mineflayer')
const { pathfinder, Movements, goals } = require('mineflayer-pathfinder')
const GoalFollow = goals.GoalFollow
const GoalBlock = goals.GoalBlock
const bot = mineflayer.createBot({
  host: '86.221.243.209', // minecraft server ip
  username: 'Bot-Clémonstre', // minecraft username
  auth: 'offline' // for offline mode servers, you can set this to 'offline'
})

bot.loadPlugin(pathfinder)

function followPlayer(){
    const playerCI = bot.players['XcoMaNdSs']
    if (!playerCI){
        bot.chat("wsh tu veut que je follow qui la")
        return
    }
    const mcData = require('minecraft-data')(bot.version)
    const movements = new Movements(bot, mcData)
    bot.pathfinder.setMovements(movements)
    movements.scafoldingBlocks =[]
    movements.scafoldingBlocks.push(mcData.blocksByName.dirt.id)

    const goal = new GoalFollow(playerCI.entity , 1)
    bot.pathfinder.setGoal(goal,true)
}

function locateIron(){
    const mcData = require('minecraft-data')(bot.version)
    const movements = new Movements(bot, mcData)
    bot.pathfinder.setMovements(movements)
    movements.scafoldingBlocks =[]
    movements.scafoldingBlocks.push(mcData.blocksByName.dirt.id)

    const IronBlock = bot.findBlock({
        matching : mcData.blocksByName.iron_ore.id,
        maxDistancce : 32
    })
    if (!IronBlock){
        bot.chat("no iron ore in 32 block")
        return
    }
    const x =IronBlock.position.x
    const y =IronBlock.position.y + 1
    const z =IronBlock.position.z
    const goalIron = new GoalBlock(x ,y ,z)
    bot.pathfinder.setGoal(goalIron)
}


bot.once('spawn',locateIron)
